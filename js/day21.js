function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);

    let [nonAllergens, candidates] = recipeAnalysis(inputText);
    let pieces = inputText.split(/\s/);
    let pt1 = 0;
    for (let food of nonAllergens.values()) {
        pt1 += pieces.filter(element => element == food).length;
    }
    console.log(`Pt1: ${pt1}`);
    let pt2 = specificAllergens(candidates);
    console.log(`Pt2: ${pt2}`);
}

function recipeAnalysis(text) {
    let allAllergens = new Set();
    let allIngredients = new Set();
    let candidates = {};
    text = text.split("\n");
    for (let line of text) {
        if (line == "") { continue; }
        let match = line.match(/(.+) \(contains (.+)\)/);
        if (!match) {
            allIngredients = new Set([...allIngredients, ...(line.split(" "))]);
            continue;
        }
        let [food, ingredients, allergens] = match;
        ingredients = ingredients.split(" ");
        allergens = allergens.split(", ");
        allAllergens = new Set([...allAllergens, ...allergens]);
        for (let allergen of allergens) {
            if (allergen in candidates) {
                candidates[allergen] = new Set(ingredients.filter(
                    x => candidates[allergen].has(x)));
            } else {
                candidates[allergen] = new Set(ingredients);
            }
        }
        allIngredients = new Set([...allIngredients, ...ingredients]);
    }
    let allCandidates = new Set();
    for (let ingredients of Object.values(candidates)) {
        allCandidates = new Set([...allCandidates, ...ingredients]);
    }
    let nonAllergens = new Set([...allIngredients].filter(
        x => !allCandidates.has(x)));
    //console.log(nonAllergens);
    //console.log(candidates);
    return [nonAllergens, candidates];
}

function specificAllergens(candidates) {
    let totalLength = Object.values(candidates).map(x => x.size).reduce((acc, val) => acc + val);
    let minLength = Object.keys(candidates).length;
    while (totalLength > minLength) {
        for (let candidate of Object.keys(candidates)) {
            if (candidates[candidate].size == 1) {
                for (let key of Object.keys(candidates)) {
                    if (candidates[key].size == 1) {
                        continue;
                    }
                    candidates[key] = new Set([...(candidates[key])]
                        .filter(x => !candidates[candidate].has(x)));
                }
            }
        }
        totalLength = Object.values(candidates).map(x => x.size).reduce((acc, val) => acc + val);
    }
    let ordered = [...(Object.keys(candidates))].sort();
    let allergens = [];
    for (let key of ordered) {
        allergens.push(candidates[key].values().next().value);
    }
    return allergens.join(",");
}

main()
