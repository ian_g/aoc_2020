function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");
    let timestamp = Number(inputText[0]);
    let buses = inputText[1].split(",");
    //timestamp = 939;
    //buses = "7,13,x,x,59,x,31,19".split(",");

    let soonestTrip = [0, timestamp+1];
    let leaveAfter;
    for (let bus of buses) {
        if (bus == "x") { continue; }
        leaveAfter = Math.ceil(timestamp/Number(bus))*Number(bus) - timestamp;
        if (leaveAfter < soonestTrip[1]) {
            soonestTrip = [Number(bus), leaveAfter];
        }
    }
    let pt1 = soonestTrip[0] * soonestTrip[1];
    console.log(`Pt1: ${pt1}`);

    let busOffsets = [];
    for (let idx = 0; idx < buses.length; idx++) {
        if (buses[idx] == "x") { continue; }
        busOffsets.push(Number(buses[idx]));
        busOffsets.push(idx);
    }

    let time = 0;
    let incrementBy = 1;
    for (let idx = 0; idx < busOffsets.length; idx += 2) {
        while ((time + busOffsets[idx+1]) % busOffsets[idx] != 0) {
            time += incrementBy;
        }
        incrementBy *= busOffsets[idx];
    }
    console.log(`Pt2: ${time}`);
}

main()
