function main() {
    const inputText = "135468729";
    //const inputText = "389125467"; //Sample input

    let cupMap = createPuzzle(inputText);
    let current = parseInt(inputText[0]);
    for (let idx = 0; idx < 100; idx++) {
        current = move(cupMap, current, 9);
    }
    console.log(`Pt1: ${puzzleString(cupMap)}`);

    cupMap = createPuzzle(inputText, true) ;
    current = parseInt(inputText[0]);
    for (let idx = 0; idx <= 10000000; idx++) {
        current = move(cupMap, current, 1000000);
    }
    let puzzleProduct = cupMap[1].next.value * cupMap[1].next.next.value;
    console.log(`Pt2: ${puzzleProduct}`);
}

main()

function createPuzzle(inputText, p2=false) {
    let order = [] ;
    let cupMap = {};
    for (let value of inputText) {
        order.push(parseInt(value));
        cupMap[parseInt(value)] = {"value": parseInt(value)};
    }
    if (p2) {
        for (let value = 10; value <= 1000000; value++) {
            order.push(value)
            cupMap[value] = {"value": value};
        }
    }
    for (let idx = 0; idx < order.length; idx++) {
        cupMap[order[idx]].next = cupMap[order[(idx+1)%order.length]];
    }
    return cupMap;
}

function puzzleString(cupMap) {
    let puzzleString = "";
    let current = cupMap[1].next;
    while (current.value != 1) {
        puzzleString += current.value;
        current = current.next;
    }
    return puzzleString;
}

function move(cupMap, current, maxValue) {
    let selection = cupMap[current].next;
    cupMap[current].next = selection.next.next.next;
    selection.next.next.next = undefined;
    let selectionValues = [
        selection.value,
        selection.next.value,
        selection.next.next.value,
    ];
    let value = current - 1;
    while (selectionValues.includes(value) || value == 0) {
        value--;
        if (value <= 0) { value = maxValue; }
    }
    let insert = cupMap[value];
    selection.next.next.next = insert.next;
    insert.next = selection;
    return cupMap[current].next.value;
}
