function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    let directions = decoder.decode(stdinData).split("\n");

    let rotateLeft = [[0, -1], [1, 0]];
    let rotateRight = [[0, 1], [-1, 0]];
    let location = [0, 0];
    let shipFaces = [1, 0];
    for (let instruction of directions) {
        if (instruction == "") { continue; }
        let direction = instruction.slice(0, 1);
        let magnitude = Number(instruction.slice(1));
        if (direction == "L") {
            for (let idx = 0; idx < magnitude/90; idx++) {
                shipFaces = rotate(shipFaces, rotateLeft);
            }
        } else if (direction == "R") {
            for (let idx = 0; idx < magnitude/90; idx++) {
                shipFaces = rotate(shipFaces, rotateRight);
            }
        } else if (direction == "F") {
            location[0] += shipFaces[0]*magnitude;
            location[1] += shipFaces[1]*magnitude;
        } else if (direction == "N") {
            location[1] += magnitude;
        } else if (direction == "S") {
            location[1] -= magnitude;
        } else if (direction == "E") {
            location[0] += magnitude;
        } else if (direction == "W") {
            location[0] -= magnitude;
        }
    }
    let pt1 = Math.abs(location[0]) + Math.abs(location[1]);
    console.log(`Pt1: ${pt1}`);

    location = [0, 0];
    let waypointOffset = [10, 1];
    for (let instruction of directions) {
        if (instruction == "") { continue; }
        let direction = instruction.slice(0, 1);
        let magnitude = Number(instruction.slice(1));
        if (direction == "L") {
            for (let idx = 0; idx < magnitude/90; idx++) {
                waypointOffset = rotate(waypointOffset, rotateLeft);
            }
        } else if (direction == "R") {
            for (let idx = 0; idx < magnitude/90; idx++) {
                waypointOffset = rotate(waypointOffset, rotateRight);
            }
        } else if (direction == "F") {
            location[0] += waypointOffset[0]*magnitude;
            location[1] += waypointOffset[1]*magnitude;
        } else if (direction == "N") {
            waypointOffset[1] += magnitude;
        } else if (direction == "S") {
            waypointOffset[1] -= magnitude;
        } else if (direction == "E") {
            waypointOffset[0] += magnitude;
        } else if (direction == "W") {
            waypointOffset[0] -= magnitude;
        }
    }
    let pt2 = Math.abs(location[0]) + Math.abs(location[1]);
    console.log(`Pt2: ${pt2}`);
}

function rotate(point, rotation) {
    if (point.length != rotation.length || point.length != rotation[0].length) {
        console.error("Point and rotation vector don't match");
        return point;
    }
    let rotated = [];
    for (let row of rotation) {
        rotated.push(row.map((val, idx) => val*point[idx]).reduce((x, y) => x + y));
    }
    return rotated;
}

main()

/*
    directions = [line.strip() for line in fileinput.input()]
    location = 0 + 0j
    ship_faces = 1

    location = 0 + 0j
    waypoint_offset = 10 + 1j
    for idx in range(len(directions)):
        direction = directions[idx][0]
        distance = int(directions[idx][1:])
        if direction == "L":
            waypoint_offset *= (1j)**(distance/90)
        elif direction == "R":
            waypoint_offset *= (-1j)**(distance/90)
        elif direction == "F":
            location += distance * waypoint_offset
        elif direction == "N":
            waypoint_offset += 1j*distance
        elif direction == "S":
            waypoint_offset += -1j*distance
        elif direction == "E":
            waypoint_offset += 1*distance
        elif direction == "W":
            waypoint_offset += -1*distance
    pt2 = abs(int(location.real)) + abs(int(location.imag))
    print(f"Pt2: {pt2}")

if __name__ == "__main__":
    day12()
*/
