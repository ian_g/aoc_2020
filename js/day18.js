function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    let inputText = decoder.decode(stdinData).split("\n");
    if (inputText[inputText.length-1] == "") {
        inputText.pop();
    }
    let prec = {
        "(": [9, true],
        ")": [0, true],
        "^": [4, false],
        "*": [3, true],
        "+": [3, true],
        "-": [2, true],
        "/": [2, true],
    };

    let pt1 = inputText.map(line => toPostfix(line, prec))
        .map(val => evaluatePostfix(val))
        .reduce((acc, val) => acc + val);
    console.log(`Pt1: ${pt1}`);
    prec["+"] = [4, true];
    let pt2 = inputText.map(line => toPostfix(line, prec))
        .map(val => evaluatePostfix(val))
        .reduce((acc, val) => acc + val);
    console.log(`Pt2: ${pt2}`);
}

function evaluatePostfix(problem) {
    problem = problem.split(/\s+/);
    let operands = [];
    for (let item of problem) {
        if (item == "") {
            continue;
        }
        if (!isNaN(parseInt(item))) {
            operands.push(parseInt(item));
        } else { //Simplified because input already verified
            let right = operands.pop();
            let left = operands.pop();
            if (item == "+") {
                operands.push(right + left);
            } else if (item == "*") {
                operands.push(right * left);
            } else if (item == "/") {
                operands.push(right / left);
            } else if (item == "-") {
                operands.push(right - left);
            } else {
                throw `Unknown operator ${item}`;
            }
        }
    }
    return operands.pop();
}

function toPostfix(problem, precedence) {
    for (let key of Object.keys(precedence)) {
        problem = problem.split(key).join(` ${key} `);
    }
    problem = problem.split(/\s+/) ;
    let outputStack = [];
    let opStack = [];
    for (let token of problem) {
        if (token == "") {
            continue;
        }
        if (token == "(") {
            opStack.push(token);
        } else if (token == ")") {
            let topToken = opStack.pop();
            while (topToken != "(") {
                outputStack.push(topToken);
                topToken = opStack.pop();
            }
        } else if (token in precedence) {
            if (opStack.length == 0) {
                opStack.push(token);
                continue;
            }
            let poppable = opStack[opStack.length-1];
            while (popoff(precedence[token], precedence[poppable]) && poppable != "(") {
                outputStack.push(opStack.pop())
                if (opStack.length == 0) {
                    break;
                }
                poppable = opStack[opStack.length-1];
            }
            opStack.push(token);
        } else {
            outputStack.push(token);
        }
    }
    while (opStack.length > 0) {
        outputStack.push(opStack.pop());
    }
    return outputStack.join(" ");
}

function popoff(tokenPrec, poppablePrec) {
    if (tokenPrec[1]) { //Left associative
        return tokenPrec[0] <= poppablePrec[0];
    }
    return tokenPrec[0] < poppablePrec[0];
}

main()
