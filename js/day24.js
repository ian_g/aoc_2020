const STEPS = {
    "w": [-2, 0],
    "e": [2, 0],
    "n": {
        "w": [-1, 1],
        "e": [1, 1],
    },
    "s": {
        "w": [-1, -1],
        "e": [1, -1],
    },
}

function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const flips = decoder.decode(stdinData).split("\n");
    let flipped = {};
    for (let line of flips) {
        if (line == "") { continue; }
        let coord = tileCoord(line);
        if (!(coord in flipped)) {
            flipped[coord] = 0;
        } 
        flipped[coord]++;
    }
    let pt1 = Object.values(flipped)
        .reduce((acc, val) => acc += (val % 2))
    console.log(`Pt1: ${pt1}`);

    let black = new Set(Object.entries(flipped)
        .filter(x => x[1] % 2 == 1 )
        .map(x => x[0]));
    //console.log(black);

    for (let idx = 0; idx < 100; idx++) {
        black = flipTiles(black);
    }
    console.log(`Pt2: ${black.size}`); 
}

function tileCoord(line) {
    let steps = STEPS;
    let coord = [0, 0];
    for (let chr of line) {
        if (chr == "n" || chr == "s") {
            steps = steps[chr];
            continue;
        }
        coord = coord.map((val, idx) => val + steps[chr][idx]);
        steps = STEPS;
    }
    return coord;
}

function flipTiles(black) {
    let coords = allNeighbors(black);
    let newBlack = new Set();
    for (let coord of coords) {
        if (score(coord, black) == 2) {
            newBlack.add(coord);
        } else if (score(coord, black) == 1 && black.has(coord)) {
            newBlack.add(coord);
        }
    }
    return newBlack;
}

function score(coord, black) {
    let adjacent = neighbors(coord);
    let blackNeighbors = new Set([...adjacent].filter(x => black.has(x)));
    return blackNeighbors.size;
}

function allNeighbors(coords) {
    let adjacent = [];
    for (let coord of coords) {
        adjacent = [...adjacent, ...neighbors(coord)];
    }
    return new Set(adjacent);
}

function neighbors(coord) {
    let adjacent = []; 
    coord = coord.split(",").map(x => Number(x));
    adjacent.push(coord.map((val, idx) => val + [-1, -1][idx]).join(","));
    adjacent.push(coord.map((val, idx) => val + [-1, 1][idx]).join(","));
    adjacent.push(coord.map((val, idx) => val + [-2, 0][idx]).join(","));
    adjacent.push(coord.map((val, idx) => val + [1, 1][idx]).join(","));
    adjacent.push(coord.map((val, idx) => val + [2, 0][idx]).join(","));
    adjacent.push(coord.map((val, idx) => val + [1, -1][idx]).join(","));
    return adjacent;
}

main()
