function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");

    let rules = {};
    for (let line of inputText) {
        if (line == "") { break; }
        let fields = line.split(/: | or |-/);
        let key = fields[0];
        rules[key] = [];
        for (let idx = Number(fields[1]); idx <= Number(fields[2]); idx++) {
            rules[key].push(idx);
        }
        for (let idx = Number(fields[3]); idx <= Number(fields[4]); idx++) {
            rules[key].push(idx);
        }
    }
    let myTicket = inputText[inputText.indexOf("your ticket:")+1].split(",");
    let nearbyTickets = inputText.slice(inputText.indexOf("nearby tickets:")+1);
    let validTickets = [myTicket];
    let pt1 = 0;
    for (let ticket of nearbyTickets) {
        if (ticket == "") {
            continue;
        }
        let [returnValue, valid] = checkTicket(ticket.split(","), rules);
        if (valid) {
            validTickets.push(ticket.split(","));
        }
        pt1 += returnValue;
    }
    console.log(`Pt1: ${pt1}`);

    let validValues = new Array(myTicket.length).fill().map(() => []);
    for (let idx = 0; idx < myTicket.length; idx++) {
        for (let ticket of validTickets) {
            validValues[idx].push(ticket[idx]);
        }
    }

    let ticketFields = [];
    for (let values of validValues) {
        ticketFields.push(possibleFields(values, rules));
    }
    ticketFields = calculateFields(ticketFields);

    let pt2 = 1;
    for (let idx = 0; idx < ticketFields.length; idx++) {
        if (ticketFields[idx].includes("departure")) {
            pt2 *= Number(myTicket[idx]); 
        }
    }
    console.log(`Pt2: ${pt2}`)
}

function calculateFields(fields) {
    let ticketFields = new Array(fields.length).fill("");
    let last = "";
    let value;
    while (ticketFields.includes("")) {
        for (let idx = 0; idx < fields.length; idx++) {
            fields[idx].delete(last);
            if (fields[idx].size == 1) {
                value = fields[idx].values().next().value;
                ticketFields[idx] = value;
            }
        }
        last = value;
    }
    return ticketFields;
}

function possibleFields(values, rules) {
    let possibleValues = new Set(Object.keys(rules));
    let impossibleValues = new Set();
    for (let [key, rule] of Object.entries(rules)) {
        for (let value of values) { 
            if (!rule.includes(Number(value))) {
                impossibleValues.add(key);
                break;
            }
        }
    }
    return new Set([...possibleValues].filter(x => !impossibleValues.has(x)));
}

function checkTicket(ticket, rules) {
    let errorValue = 0;
    let errorFound = false;
    for (let value of ticket) {
        value = Number(value);
        let valid = false;
        for (let rule of Object.values(rules)) {
            valid = valid || rule.includes(value);
        }
        if (!valid) {
            errorFound = true;
            errorValue += value;
        }
    }
    return [errorValue, !errorFound];
}

main()
