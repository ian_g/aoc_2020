function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");

    let containedBy = {};
    let contains = {};
    let container;
    let contents;
    for (let rule of inputText) {
        if (rule == "") { continue; }
        [container, contents] = parseRule(rule);
        contains[container] = contents;
        for (let bag of contents) {
            try {
                containedBy[bag[1]].add(container);
            } catch(err) {
                containedBy[bag[1]] = new Set([container]);
            }
        }
    }
    //console.log(contains);
    //console.log(containedBy);
    let pt1 = allContainers("shiny gold", containedBy).size;
    let pt2 = bagsWithin("shiny gold", contains);
    console.log(`Pt1: ${pt1}`);
    console.log(`Pt2: ${pt2}`);

}

function parseRule(rule) {
    let container = rule.substring(0, rule.indexOf(" bag"));
    let contents = [];
    for (let bag of rule.split(" contain ")[1].split(", ")) {
        if (bag.substring(0, 2) == "no") { continue; }
        contents.push([Number([bag[0]]), bag.substring(2).split(" bag")[0]]);
    }
    return [container, contents];
}

function allContainers(container, rules) {
    let allBags = new Set(); 
    if (rules[container] == undefined) {
        return new Set();
    }
    for (let value of rules[container]) {
        allBags = new Set([...allBags, ...allContainers(value, rules)]);
    }
    return new Set([...allBags, ...rules[container]]);
}

function bagsWithin(container, rules) {
    let count = 0;
    for (let rule of rules[container]) {
        count += rule[0];
        count += rule[0] * bagsWithin(rule[1], rules);
    }
    return count;
}

main()
