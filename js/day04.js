function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);
    let passports = inputText.split("\n\n");

    let hasReqFields = 0;
    let hasValidData = 0;
    let requiredFields = ["byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"];
    for (let passport of passports) {
        let keys = passport.split(/\s/)
            .map(field => field.split(":")[0])
            .filter(field => field != "cid" && field != "")
            .sort();
        if (equalArray(keys, requiredFields)) {
            hasReqFields++;
            hasValidData += validData(passport);
        }
    }

    console.log(`Pt1: ${hasReqFields}`);
    console.log(`Pt2: ${hasValidData}`);
}

function equalArray(arr1, arr2) {
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) { return false; }
    if (arr1.length != arr2.length) { return false; }
    for (let idx = 0; idx < arr1.length; idx++) {
        if (arr1[idx] !== arr2[idx]) { return false; }
    }
    return true;
}

function validData(data) {
    for (let pair of data.split(/\s/)) {
        let [key, val] = pair.split(":");

        switch (key) {
            case "byr":
                if (!("1920" <= val) || !(val <= "2002")) { return false; }
                break;
            case "ecl":
                let validEyes = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
                if (!validEyes.includes(val)) { return false; }
                break;
            case "eyr":
                if (!("2020" <= val) || !(val <= "2030")) { return false; }
                break;
            case "hcl":
                let invalidHex = isNaN(parseInt(val.substring(1), 16));
                if (val[0] != "#" || invalidHex) { return false; }
                break;
            case "hgt":
                if (!validateHeight(val)) { return false; }
                break;
            case "iyr":
                if (!("2010" <= val) || !(val <= "2020")) { return false; }
                break;
            case "pid":
                if (val.length != 9 || isNaN(Number(val))) { return false; }
                break;
            default:
                continue;
        }
    }
    return true;
}

function validateHeight(height) {
    let heightValue;
    if (height.includes("cm")) {
        heightValue = height.split("cm")[0]
        return ("150" <= heightValue) && (heightValue <= "193");
    } else if (height.includes("in")) {
        heightValue = height.split("in")[0]
        return ("59" <= heightValue) && (heightValue <= "76");
    }
    return false;
}

main()
