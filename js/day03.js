function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");

    let treeCount = sledCollisions(1, 3, inputText);
    let treeProduct = 1;
    treeProduct *= sledCollisions(1, 1, inputText);
    treeProduct *= sledCollisions(1, 3, inputText);
    treeProduct *= sledCollisions(1, 5, inputText);
    treeProduct *= sledCollisions(1, 7, inputText);
    treeProduct *= sledCollisions(2, 1, inputText);
    console.log(`Pt 1: ${treeCount}`);
    console.log(`Pt 2: ${treeProduct}`);

}

function sledCollisions(down, right, text) {
    let downLocn = 0;
    let rightLocn = 0;
    let count = 0;
    while (downLocn < text.length) {
        if (text[downLocn][rightLocn] == "#") { count++; }
        rightLocn += right;
        rightLocn %= text[0].length;
        downLocn += down;
    }
    return count;
}

main()
