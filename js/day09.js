function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");

    let preambleLength = 25;
    //preambleLength = 5;

    let numbers = [];
    for (let idx = 0; idx < inputText.length; idx++) {
        if (!isNaN(Number(inputText[idx]))) {
            numbers.push(Number(inputText[idx]));
        }
    }

    let pt1 = firstInvalid(numbers, preambleLength);
    let pt2 = weaknessSum(numbers, pt1);
    console.log(`Pt1: ${pt1}\nPt2: ${pt2}`);
}

function firstInvalid(numbers, length) {
    let valid;
    for (let idx = length; idx < numbers.length; idx++) {
        //console.log(`IDX: ${idx}`);
        //console.log(numbers.slice(idx-length, idx));
        valid = false;
        for (let pair of enumeratePairs(numbers.slice(idx-length, idx))) {
            if (pair[0] + pair[1] == numbers[idx]) {
                valid = true;
            }
        }
        if (!valid) {
            return numbers[idx];
        }
    }
    return -1;
}

function enumeratePairs(numbers) {
    let pairs = [];
    for (let idx1 = 0; idx1 < numbers.length; idx1++) {
        for (let idx2 = idx1+1; idx2 < numbers.length; idx2++) {
            pairs.push([numbers[idx1], numbers[idx2]]);
        }
    }
    return pairs;
}

function weaknessSum(numbers, value) {
    let seqSum;
    let offset;
    let seq;
    for (let idx = 0; idx < numbers.length; idx++) {
        seqSum = 0;
        offset = 1;
        //console.log(`IDX: ${idx}`);
        while (seqSum <= value) {
            seq = numbers.slice(idx, idx+offset);
            //console.log(seq);
            seqSum = seq.reduce((acc, current) => acc + current);
            if (seqSum == value) {
                return Math.max(...seq) + Math.min(...seq); 
            }
            offset++;
        } 
    }
    return -1;
}

main()
