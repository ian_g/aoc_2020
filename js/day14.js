function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");
    let program = [];
    for (let line of inputText) {
        if (line == "") { continue; }
        else if (line.substring(0, 4) == "mask") {
            program.push(["mask", line.substring(7)]);
        } else {
            program.push([
                Number(line.split("[")[1].split("]")[0]),
                Number(line.split(" = ")[1])
            ]);
        }
    }

    let memory = runV1(program);
    let pt1 = Object.values(memory).reduce((acc, val) =>
        isNaN(Number(val)) ? acc : acc + Number(val)
    );
    console.log(`Pt1: ${pt1}`);

    memory = runV2(program);
    //console.log(memory);
    let pt2 = Object.values(memory).reduce((acc, val) =>
        isNaN(Number(val)) ? acc : acc + Number(val)
    );
    console.log(`Pt2: ${pt2}`);
}

function runV1(program) {
    let memory = {};
    let mask = "";
    let memval;
    for (let instruction of program) {
        if (instruction[0] == "mask") {
            mask = instruction[1].split("");
            continue;
        }
        memval = Number(instruction[1]).toString(2);
        memval = ("000000000000000000000000000000000000" + memval).substr(-36, 36);
        memval = memval.split("");
        for (let idx = 0; idx < mask.length; idx++) {
            if (mask[idx] == "X") { continue; }
            memval[idx] = mask[idx];
        }
        memval = parseInt(memval.join(""), 2);
        memory[instruction[0]] = memval; 
    }
    return memory;
}

function runV2(program) {
    let memory = {};
    let mask = "";
    let memval;
    let memadd;
    for (let instruction of program) {
        if (instruction[0] == "mask") {
            mask = instruction[1].split("");
            continue;
        }
        memval = instruction[1];
        memadd = Number(instruction[0]).toString(2);
        memadd = ("000000000000000000000000000000000000" + memadd).substr(-36, 36);
        memadd = memadd.split("");
        for (let idx = 0; idx < mask.length; idx++) {
            if (mask[idx] == "0") { continue; }
            memadd[idx] = mask[idx];
        }
        writeTo(memadd, memval, memory);
    }
    return memory;
}

function writeTo(address, value, memory) {
    let idx = address.indexOf("X");
    if (idx < 0) {
        address = parseInt(address.join(""), 2);
        memory[address] = value;
        return;
    }
    let addressOne = [...address];
    addressOne[idx] = "1";
    let addressZero = [...address];
    addressZero[idx] = "0";
    writeTo(addressOne, value, memory);
    writeTo(addressZero, value, memory);
    return;
}

main()
