function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);
    let [cardPub, doorPub, ...eh] = inputText.split("\n"); 
    //cardPub = 5764801;
    //doorPub = 17807724;
    let cardLoop = determineLoop(cardPub);
    let doorLoop = determineLoop(doorPub);

    let pt1 = transform(cardLoop, doorPub);
    console.log(`Pt1: ${pt1}`);

}

function transform(loopSize, subjectNumber) {
    let loop = 0;
    let value = 1;
    while (loop < loopSize) {
        loop++;
        value *= subjectNumber;
        value %= 20201227;
    }
    return value;
}

function determineLoop(pubKey, subjectNumber=7) {
    let loopSize = 0;
    let value = 1;
    while (value != pubKey) {
        value *= subjectNumber;
        value %= 20201227;
        loopSize++;
    }
    return loopSize;
}

main()
