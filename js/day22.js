function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    let [p1, p2] = decoder.decode(stdinData).split("\n\n");
    let deck1 = p1.split("\n")
        .filter(x => !isNaN(Number(x)) && x != "")
        .map(x => Number(x));
    let deck2 = p2.split("\n")
        .filter(x => !isNaN(Number(x)) && x != "")
        .map(x => Number(x));
    combat(deck1, deck2);
    let winner = deck1;
    if (deck1.length == 0) {
        winner = deck2;
    }
    let score = winner.reduce((acc, val, idx) => 
        acc + val*(winner.length-idx), 0);
    console.log(`Pt1: ${score}`);

    deck1 = p1.split("\n")
        .filter(x => !isNaN(Number(x)) && x != "")
        .map(x => Number(x));
    deck2 = p2.split("\n")
        .filter(x => !isNaN(Number(x)) && x != "")
        .map(x => Number(x));
    combat(deck1, deck2, true);
    winner = deck1;
    if (deck1.length == 0) {
        winner = deck2;
    }
    score = winner.reduce((acc, val, idx) =>
        acc + val*(winner.length-idx), 0);
    console.log(`Pt2: ${score}`);
}

function combat(deck1, deck2, recursive=false) {
    let rounds = [];
    while (deck1.length > 0 && deck2.length > 0) {
        if (recursive && rounds.includes(deck1.toString() + "|" + deck2.toString())) {
            return true;
        } else if (recursive) {
            rounds.push(deck1.toString() + "|" + deck2.toString());
        }
        let card1 = deck1.shift();
        let card2 = deck2.shift();

        let p1win;
        if (recursive && deck1.length >= card1 && deck2.length >= card2) {
            p1win = combat(deck1.slice(0, card1), deck2.slice(0, card2), true);
        } else {
            p1win = (card1 > card2);
        }
        
        if (p1win) {
            deck1.push(card1);
            deck1.push(card2);
        } else {
            deck2.push(card2);
            deck2.push(card1);
        }
    }
    return (deck1.length != 0); //true if deck1 wins
}

main()
