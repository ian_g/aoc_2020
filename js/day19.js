function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");
    let rules = new Array(136).fill("");
    let startFrom = 0;
    for (let idx = 0; idx < inputText.length; idx++) {
        if (inputText[idx] == "") {
            startFrom = idx;
            break;
        }
        let [number, values] = inputText[idx].split(": ");
        rules[Number(number)] = values;
    }
    let ruleZero = "^" + solveRules(0, rules) + "$";
    let pt1 = 0;
    for (let idx = startFrom; idx < inputText.length; idx++) {
        if (inputText[idx] == "") { continue; }
        if (inputText[idx].match(ruleZero)) { pt1++; }
    }
    console.log(`Pt1: ${pt1}`);
    ruleZero = "^" + solveRules(0, rules, true) + "$";
    let pt2 = 0;
    for (let idx = startFrom; idx < inputText.length; idx++) {
        if (inputText[idx] == "") { continue; }
        if (inputText[idx].match(ruleZero)) { pt2++; }
    }
    console.log(`Pt2: ${pt2}`);
}

function solveRules(index, rules, p2=false) {
    let ruleIndex = rules[index];
    if (ruleIndex.includes("\"")) {
        return ruleIndex.split("\"")[1];
    }
    let rule = [];
    if (index == 8 && p2) {
        return solveRules(42, rules) + "+";
    } else if (index == 11 && p2) {
        let fortyTwo = solveRules(42, rules);
        let thirtyOne = solveRules(31, rules);
        for (let idx = 1; idx < 11; idx++) {
            rule.push(`${fortyTwo}{${idx}}${thirtyOne}{${idx}}`);
        }
        return "(" + rule.join("|") + ")";
    }
    for (let possibility of ruleIndex.split(" | ")) {
        let value = "";
        for (let number of possibility.split(" ")) {
            value += solveRules(Number(number), rules, p2);
        }
        rule.push(value);
    }
    return "(" + rule.join("|") + ")";
}

main()
