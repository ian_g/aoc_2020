function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    let text = decoder.decode(stdinData).split("\n");
    let grid = [];
    let grid2 = [];
    for (let line of text) {
        if (line == "") {
            continue;
        }
        grid.push(line.split(""));
        grid2.push(line.split(""));
    }

    
    let filled = filledSeats(grid);
    grid = cycle(grid);
    while (filled != filledSeats(grid)) {
        filled = filledSeats(grid);
        grid = cycle(grid);
    }
    console.log(`Pt1: ${filled}`);

    filled = filledSeats(grid2);
    grid = cycleInView(grid2);
    while (filled != filledSeats(grid)){
        filled = filledSeats(grid);
        grid = cycleInView(grid);
    }
    console.log(`Pt2: ${filled}`);
}

function filledSeats(grid) {
    return grid.flat()
        .reduce((total, value) => (
        total + ((value == "#") ? 1 : 0)), 0);
}

function cycleInView(grid) {
    let cycled = [];
    let rLim = grid.length - 1;
    let cLim = grid[0].length - 1;
    let score;
    for (let row = 0; row < grid.length; row++) {
        let cycledRow = [];
        for (let col = 0; col < grid[0].length; col++) {
            if (grid[row][col] == ".") {
                cycledRow.push(".");
                continue;
            }
            score = 0;
            score += inView(grid, row-1, col-1, -1, -1);
            score += inView(grid, row-1, col, -1, 0);
            score += inView(grid, row, col-1, 0, -1);
            score += inView(grid, row-1, col+1, -1, +1);
            score += inView(grid, row, col+1, 0, +1);
            score += inView(grid, row+1, col-1, +1, -1);
            score += inView(grid, row+1, col, +1, 0);
            score += inView(grid, row+1, col+1, +1, +1);
            if (grid[row][col]  == "L" && score == 0) {
                cycledRow.push("#");
                continue;
            }
            if (grid[row][col] == "#" && score > 4) {
                cycledRow.push("L");
                continue;
            }
            cycledRow.push(grid[row][col]);
        }
        cycled.push(cycledRow);
    }
    return cycled;
}

function inView(grid, row, col, dRow, dCol) {
    if (row < 0 || col < 0 || row > grid.length-1 || col > grid[0].length-1) {
        return false;
    }
    if (grid[row][col] == "L") {
        return false;
    }
    if (grid[row][col] == "#") {
        return true;
    }
    return inView(grid, row+dRow, col+dCol, dRow, dCol);
}

function cycle(grid) {
    let cycled = [];
    let rLim = grid.length - 1;
    let cLim = grid[0].length - 1;
    let score;
    for (let row = 0; row < grid.length; row++) {
        let cycledRow = [];
        for (let col = 0; col < grid[0].length; col++) {
            if (grid[row][col] == ".") {
                cycledRow.push(".");
                continue;
            }
            score = 0;
            score += (row > 0 && col > 0 && grid[row-1][col-1] == "#");
            score += (row > 0 && grid[row-1][col] == "#");
            score += (col > 0 && grid[row][col-1] == "#");
            score += (row > 0 && col < cLim && grid[row-1][col+1] == "#");
            score += (col < cLim && grid[row][col+1] == "#");
            score += (row < rLim && col > 0 && grid[row+1][col-1] == "#");
            score += (row < rLim && grid[row+1][col] == "#");
            score += (row < rLim && col < cLim && grid[row+1][col+1] == "#");
            if (grid[row][col]  == "L" && score == 0) {
                cycledRow.push("#");
                continue;
            }
            if (grid[row][col] == "#" && score > 3) {
                cycledRow.push("L");
                continue;
            }
            cycledRow.push(grid[row][col]);
        }
        cycled.push(cycledRow);
    }
    return cycled;
}

main()
