const passwordSplitter = /(\d+)-(\d+) (.): (.+)/;
function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);

    let passwords = inputText.split("\n");
    if (passwords[passwords.length-1] == "") {
        passwords.pop();
    }
    let validSledPasswords = 0;
    let validTobogganPasswords = 0;
    let match;
    for (let pw of passwords) {
        match = pw.match(passwordSplitter);
        if (!match) {
            console.log(pw);
        }
        if (validSledPassword(pw)) { validSledPasswords++; }
        if (validTobogganPassword(pw)) { validTobogganPasswords++; }
    }
    console.log(`Pt1: ${validSledPasswords}`);
    console.log(`Pt2: ${validTobogganPasswords}`);

}

function validSledPassword(password) {
    let match = password.match(passwordSplitter);
    let [ , low, high, letter, pw] = match;

    let count = (pw.match(new RegExp(letter, "g"))||[]).length;
    //let count = pw.split(letter) - 1;
    return (count >= low) && (count <= high);
}

function validTobogganPassword(password) {
    let match = password.match(passwordSplitter);
    let [ , low, high, letter, pw] = match;
    
    return (pw[Number(low)-1] == letter) ^ (pw[Number(high)-1] == letter);
}

main()
