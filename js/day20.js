function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData);
    let tiles = inputText.split("\n\n");
    let tileData = {};
    let allEdges = [];
    for (let tile of tiles) {
        let [title, image] = tile.split(":\n");
        let idNumber = title.split(" ")[1];
        let tileEdges = [];
        let rows = image.split("\n");
        tileEdges.push(rows[0]);
        tileEdges.push(rows[rows.length-1]);
        tileEdges.push(rows.map(x => x[0]).join(""));
        tileEdges.push(rows.map(x => x[x.length-1]).join(""));
        tileData[parseInt(idNumber)] = tileEdges;
        allEdges = allEdges.concat(tileEdges);
    }
    let cornerTiles = new Set();
    for (let tile of Object.keys(tileData)) {
        let edgeCount = 0;
        for (let edge of tileData[tile]) {
            edgeCount += allEdges.filter(x => x == edge).length;
            let egde = edge.split("").reverse().join("");
            edgeCount += allEdges.filter(x => x == egde).length;
        }
        if (edgeCount < 7) {
            cornerTiles.add(tile);
        }
    }
    let pt1 = 1;
    for (let val of cornerTiles) {
        pt1 *= val;
    }
    //console.log(cornerTiles);
    console.log(`Pt1: ${pt1}`);
}

main()
