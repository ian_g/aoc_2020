function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");

    let active = initialActive(inputText, 3);
    active = cycles(active, 6);
    console.log(`Pt1: ${active.size}`);

    active = initialActive(inputText, 4);
    active = cycles(active, 6);
    console.log(`Pt2: ${active.size}`)
}

function cycles(active, cycleCount) {
    for (let idx = 0; idx < cycleCount; idx++) {
        let neighbors = new Set();
        for (let point of active) {
            neighbors = new Set([...neighbors, ...getNeighbors(point)]);
        }
        neighbors = [...neighbors].filter(val => !active.has(val));
        let newActive = new Set();
        for (let neighbor of neighbors) {
            if (activates(neighbor, false, active)) {
                newActive.add(neighbor);
            }
        }
        for (let point of active) {
            if (activates(point, true, active)) {
                newActive.add(point);
            }
        }
        active = newActive;
    }
    return active;
}

function activates(point, isActive, active) {
    let score = 0;
    let neighbors = getNeighbors(point);
    for (let locn of active) {
        if (locn == point) {
            continue;
        }
        if (score == 4) {
            return false;
        }
        if (neighbors.has(locn)) {
            score++;
        }
    }
    if (score == 3) {
        return true;
    }  else if (score == 2 && isActive) {
        return true;
    }
    return false;
}

function getNeighbors(point) { //Includes self
    let neighbors = new Set();
    let coord = toCoord(point);
    let deltas = generateDeltas([-1, 0, 1], coord.length);
    for (let delta of deltas) {
        let neighbor = coord.map((val, idx) => val + delta[idx]);
        neighbors.add(neighbor.toString());
    }
    return neighbors;
}

function generateDeltas(values, length) {
    let deltas = [[]];
    let newDeltas;
    while (deltas[0].length < length) {
        newDeltas = [];
        for (let base of deltas) {
            for (let value of values) {
                newDeltas.push([...base, value]);
            }
        }
        deltas = newDeltas;
    }
    return deltas;
}

function toCoord(point) {
    let coord = point.split(",");
    for (let idx = 0; idx < coord.length; idx++) {
        coord[idx] = Number(coord[idx]);
    }
    return coord
}

function initialActive(inputText, dimensions) {
    let active = new Set();
    for (let idxX = 0; idxX < inputText.length; idxX++) {
        if (inputText[idxX] == "") {
            continue;
        }
        for (let idxY = 0; idxY < inputText.length; idxY++) {
            if (inputText[idxX][idxY] == "#") {
                let point = new Array(dimensions).fill(0);
                point[0] = idxX;
                point[1] = idxY;
                active.add(point.toString());
            }
        }
    }
    return active;
}

main()
