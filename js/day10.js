function main() {
    const decoder = new TextDecoder("utf-8");
    const stdinData = Deno.readAllSync(Deno.stdin);
    const inputText = decoder.decode(stdinData).split("\n");

    let numbers = [];
    for (let idx = 0; idx < inputText.length; idx++) {
        if (!isNaN(Number(inputText[idx]))) {
            numbers.push(Number(inputText[idx]));
        }
    }
    //numbers.push(0); //Empty line at end becomes 0 above
    numbers.push(Math.max(...numbers) + 3);
    numbers.sort((a, b) => a - b);

    let ones = 0;
    let threes = 0;
    for (let idx = 1; idx < numbers.length; idx++) {
        if (numbers[idx]-numbers[idx-1] == 1){
            ones++;
        } else if (numbers[idx] - numbers[idx-1] == 3){
            threes++;
        }
    }

    let pt1 = ones * threes;
    let pt2 = countPaths(numbers);
    console.log(`Pt1: ${pt1}\nPt2: ${pt2}`);
}

function countPaths(numbers) {
    let pathCounts = new Array(numbers[numbers.length-1] + 1).fill(0); 
    pathCounts[0] = 1;
    for (let val of numbers){ 
        if (val == 0) {
            continue;
        } else if (val == 1) {
            pathCounts[1] += pathCounts[0];
            continue;
        } else if (val == 2) {
            pathCounts[2] += pathCounts[1] + pathCounts[0];
            continue;
        }
        pathCounts[val] += pathCounts[val-1] + pathCounts[val-2] + pathCounts[val-3];
    }
    //console.log(pathCounts);
    return pathCounts[pathCounts.length-1];
}

main()
