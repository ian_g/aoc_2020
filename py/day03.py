#!/usr/bin/env python
import sys

def day03():
    input_text = [line[:-1] for line in sys.stdin]

    #Sample input from instructions
    #input_text = [
    #    "..##.......",
    #    "#...#...#..",
    #    ".#....#..#.",
    #    "..#.#...#.#",
    #    ".#...##..#.",
    #    "..#.##.....",
    #    ".#.#.#....#",
    #    ".#........#",
    #    "#.##...#...",
    #    "#...##....#",
    #    ".#..#...#.#",
    #]

    tree_count = sled_slope(1, 3, input_text)
    print(f"Pt 1: {tree_count}")
    tree_product = 1
    tree_product *= sled_slope(1, 1, input_text)
    tree_product *= sled_slope(1, 3, input_text)
    tree_product *= sled_slope(1, 5, input_text)
    tree_product *= sled_slope(1, 7, input_text)
    tree_product *= sled_slope(2, 1, input_text)
    print(f"Pt 2: {tree_product}")

def sled_slope(down, right, text):
    down_locn = 0
    right_locn = 0
    count = 0
    while down_locn < len(text):
        #print(down_locn, right_locn)
        if text[down_locn][right_locn] == "#":
            count += 1
        right_locn += right
        right_locn %= len(text[0])
        down_locn += down
    return count

if __name__ == "__main__":
    day03()
