#!/usr/bin/env python
import sys

def day10():
    text = [int(line.strip()) for line in sys.stdin]
    #Joltage
    #Adapters rated for specific output
    #   Can take input 1, 2, or 3 jolts lower and produce correct output
    #Device rated for 3 jolts higher than highest-rated adapter in bag
    #Charging outlet: effective joltage of 0
    text.append(0) #Outlet
    text.append(max(text) + 3) #Device
    text.sort()
    ones = 0
    threes = 0
    for idx in range(1, len(text)):
        if text[idx] - text[idx-1] == 1:
            ones += 1
        elif text[idx] - text[idx-1] == 3:
            threes += 1
    pt1 = ones * threes
    print(f"Pt1: {pt1}")

    pt2 = enumerate_paths(text)
    print(f"Pt2: {pt2}")

def enumerate_paths(text):
    path_count = [1] + [0 for _ in range(max(text))]
    for val in text:
        if val == 1:
            path_count[1] += path_count[0]
            continue
        elif val == 2:
            path_count[2] += path_count[0] + path_count[1]
            continue
        path_count[val] += path_count[val-1] + path_count[val-2] + path_count[val-3]
    return path_count[-1]
    
if __name__ == "__main__":
    day10()
