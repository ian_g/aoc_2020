#!/usr/bin/env python
import re 
import sys

def day19():
    input_text = [line.strip() for line in sys.stdin]
    rules = [""]*136
    start_from = 0
    for idx in range(len(input_text)):
        if input_text[idx] == "":
            start_from = idx
            break
        [number, values] = input_text[idx].split(": ")
        rules[int(number)] = values 
    rule_zero = solve_rules(0, rules)
    rule_zero = "^" + rule_zero + "$"

    pt1 = 0
    for idx in range(start_from, len(input_text)):
        if input_text[idx] == "":
            continue
        if re.match(rule_zero, input_text[idx]):
            pt1 += 1
    print(f"Pt1: {pt1}")

    rule_zero = solve_rules(0, rules, True)
    pt2 = 0
    for idx in range(start_from, len(input_text)):
        if input_text[idx] == "":
            continue
        if re.fullmatch(rule_zero, input_text[idx]):
            pt2 += 1
    print(f"Pt2: {pt2}")

def solve_rules(index, rules, p2=False):
    rule_index = rules[index]
    if "\"" in rule_index:
        return rule_index.split("\"")[1]
    rule = []

    if index == 8 and p2:
        return solve_rules(42, rules) + "+"
    elif index == 11 and p2:
        forty_two = solve_rules(42, rules)
        thirty_one = solve_rules(31, rules)
        base = [f"{forty_two}{{{x}}}{thirty_one}{{{x}}}" for x in range(1, 11)]
        return "(" + "|".join(base) + ")"
    for possibility in rule_index.split(" | "):
        value = ""
        for number in possibility.split():
            value += solve_rules(int(number), rules, p2)
        rule.append(value)
    return "(" + "|".join(rule) + ")"

if __name__ == "__main__":
    day19()
