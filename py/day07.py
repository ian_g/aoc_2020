#!/usr/bin/env python
import sys
import collections

def day07():
    rules_contained_by = collections.defaultdict(set)
    rules_contains = {}
    for line in sys.stdin:
        container, contents = parse_line(line)
        rules_contains[container] = contents
        for line in contents:
            rules_contained_by[line[1]].add(container)
    pt1 = len(all_containers("shiny gold", rules_contained_by))
    print(f"Pt1: {pt1}")
    pt2 = bags_within("shiny gold", rules_contains)
    print(f"Pt2: {pt2}")

def bags_within(container, rules):
    count = 0
    for rule in rules[container]:
        count += rule[0]
        count += rule[0]*bags_within(rule[1], rules)
    return count

def all_containers(container, rules):
    containers = rules[container]
    all_bags = containers | set() 
    for value in containers:
        all_bags = all_bags | all_containers(value, rules)
    return all_bags | containers 

def parse_line(line):
    container = line.split(" contain ")[0].split(" bag")[0]
    contents_text = line.split(" contain ")[1].split(", ")
    contents = []
    for bag in contents_text:
        try:
            contents.append([
                int(bag[0]),
                bag[2:].split(" bag")[0]
            ])
        except ValueError:
            continue
    return container, contents

if __name__ == "__main__":
    day07()
