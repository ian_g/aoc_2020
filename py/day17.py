#!/usr/bin/env python
import sys 
import itertools

def day17():
    input_text = [line.strip() for line in sys.stdin]
        
    active = {(idx_x, idx_y, 0) for idx_x in range(len(input_text))
        for idx_y, char in enumerate(input_text[idx_x])
        if char == "#"}
    active = cycles(active, 6)

    pt1 = len(active)
    print(f"Pt1: {pt1}")

    active = {(idx_x, idx_y, 0, 0) for idx_x in range(len(input_text))
        for idx_y, char in enumerate(input_text[idx_x])
        if char == "#"}
    active = cycles(active, 6)

    pt2 = len(active)
    print(f"Pt2: {pt2}")

def get_neighbors(point):
    neighbors = set()
    for delta in itertools.product([-1,0,1], repeat=len(point)):
        neighbors.add(tuple(pt + dt for pt, dt in zip(point, delta)))
    return neighbors

def activates(point, is_active, active):
    score = 0
    neighbors = get_neighbors(point)
    for locn in active:
        if locn == point:
            continue
        if score == 4:
            return False
        if locn in neighbors:
            score += 1
    if score == 3:
        return True
    elif score == 2 and is_active:
        return True
    return False

def cycles(active, cycle_count):
    for _ in range(cycle_count):
        neighbors = set()
        neighbors = set.union(*(get_neighbors(point) for point in active))
        neighbors = neighbors - active
        new_active = set()
        for neighbor in neighbors:
            if activates(neighbor, False, active):
                new_active.add(neighbor)
        for point in active:
            if activates(point, True, active):
                new_active.add(point)
        active = new_active
    return active

if __name__ == "__main__":
    day17()
