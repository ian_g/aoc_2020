#!/usr/bin/env python
import sys

def day22():
    p1, p2 = sys.stdin.read().split("\n\n")
    deck1 = [int(val.strip()) for val in p1.splitlines()[1:]]
    deck2 = [int(val.strip()) for val in p2.splitlines()[1:]]
    combat(deck1, deck2)
    if len(deck1) > 0:
        winner = deck1
    else:
        winner = deck2
    pt1 = 0
    cards = len(winner)
    for idx in range(cards):
        pt1 += winner[idx]*(cards-idx)
    print(f"Pt1: {pt1}")

    deck1 = [int(val.strip()) for val in p1.splitlines()[1:]]
    deck2 = [int(val.strip()) for val in p2.splitlines()[1:]]
    p1 = recursive_combat(deck1, deck2)
    if p1: 
        winner = deck1
    else:
        winner = deck2
    pt2 = 0
    cards = len(winner)
    for idx in range(cards):
        pt2 += winner[idx]*(cards-idx)
    print(f"Pt2: {pt2}")

def recursive_combat(deck1, deck2):
    rounds = []
    while deck1 and deck2:
        if (deck1, deck2) in rounds:
            return True
        rounds.append(([*deck1], [*deck2]))
        card1 = deck1.pop(0)
        card2 = deck2.pop(0)
        if (card1, card2) in rounds:
            return True

        if len(deck1) >= card1 and len(deck2) >= card2:
            #win1 = recursive_combat(deck1.copy()[:card1], deck2.copy()[:card2])
            win1 = recursive_combat([*deck1][:card1], [*deck2][:card2])
        else:
            win1 = card1 > card2

        if win1:
            deck1 += [card1, card2]
        else:
            deck2 += [card2, card1]
    return len(deck1) != 0 #True if deck1 wins

def combat(deck1, deck2):
    while len(deck1) > 0 and len(deck2) > 0:
        if deck1[0] > deck2[0]:
            win = deck1.pop(0)
            lose = deck2.pop(0)
            deck1.append(win)
            deck1.append(lose)
        else:
            win = deck2.pop(0)
            lose = deck1.pop(0)
            deck2.append(win)
            deck2.append(lose)

if __name__ == "__main__":
    day22()
