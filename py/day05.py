#!/usr/bin/env python
import sys

def day05():
    seat_ids = []
    for line in sys.stdin:
        seat_id = int(line.strip().replace("F", "0")
            .replace("B", "1")
            .replace("L", "0")
            .replace("R", "1"), 2)
        seat_ids.append(seat_id)

    seat_ids.sort()
    print(f"Pt1: {seat_ids[-1]}")

    for idx in range(25, len(seat_ids) - 1):
        if seat_ids[idx] == (seat_ids[idx-1] + 1):
            continue
        #Adjust down 1, since we're told there's a gap of two
        #and this is the higher one of those
        print(f"Pt2: {seat_ids[idx]-1}") 
        return

if __name__ == "__main__":
    day05()
