#!/usr/bin/env python
import sys
import itertools 

def day09():
    input_text = [int(line.strip()) for line in sys.stdin]
    #preamble_length = 5
    preamble_length = 25
    pt1 = first_invalid(input_text, preamble_length)
    print(f"Pt1: {pt1}")
    pt2 = weakness_sum(pt1, input_text)
    print(f"Pt2: {pt2}")

def first_invalid(numbers, length):
    for idx in range(length, len(numbers)):
        match = False
        for pair in itertools.combinations(numbers[idx-length:idx], 2):
            if sum(pair) == numbers[idx]:
                match = True
        if not match:
            return numbers[idx]
    return -1 

def weakness_sum(value, numbers):
    idx = 0
    for idx in range(len(numbers)):
        seq_sum = 0
        offset = 1
        while seq_sum <= value:
            seq = numbers[idx:idx+offset]
            seq_sum = sum(seq)
            if seq_sum == value:
                return max(seq) + min(seq)
            offset += 1
    return -1

if __name__ == "__main__":
    day09()
