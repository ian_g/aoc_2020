#!/usr/bin/env python
import sys
MODULUS = 20201227

def day25():
    [card_pub, door_pub] = [int(line) for line in sys.stdin]
    #Sample data
    #card_pub = 5764801
    #door_pub = 17807724
    card_loop = determine_loop_size(card_pub)
    door_loop = determine_loop_size(door_pub)
    #print(f"Card: {card_loop}\nDoor: {door_loop}")

    pt1 = pow(door_pub, card_loop, MODULUS)
    print(f"Pt1: {pt1}")

def determine_loop_size(pub_key, subject_number=7):
    loop_size = 0
    value = 1
    while value != pub_key:
        value *= subject_number 
        value %= MODULUS 
        loop_size += 1
    return loop_size

if __name__ == "__main__":
    day25()
