#!/usr/bin/env python
import sys
import collections

def day08():
    input_text = [line.strip() for line in sys.stdin]
    [pt1, _] = run_program(input_text)
    print(f"Pt1: {pt1}")

    for idx in range(len(input_text)):
        prog = input_text.copy()
        if "nop" in input_text[idx]:
            prog[idx] = "jmp " + input_text[idx][4:]
            [pt2, end] = run_program(prog)
        elif "jmp" in input_text[idx]:
            prog[idx] = "nop " + input_text[idx][4:]
            [pt2, end] = run_program(prog)
        if end == None:
            break
    print(f"Pt2: {pt2}")

def run_program(program):
    instructions_run = []
    idx = 0
    program_value = 0
    while idx < len(program):
        if idx in instructions_run:
            return [program_value, idx]
        instructions_run.append(idx)
        [instr, value] = program[idx].split()
        value = int(value)
        if instr == "acc":
            program_value += value
        elif instr == "jmp":
            idx += value
            continue #Skip index increment
        idx += 1
    return [program_value, None]

if __name__ == "__main__":
    day08()
