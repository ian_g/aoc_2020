#!/usr/bin/env python
import sys

def day20():
    tiles = sys.stdin.read().split("\n\n")
    tile_data = {}
    all_edges = []
    for tile in tiles:
        title, image = tile.split(":\n")
        id_number = title.split()[1]
        tile_edges = []
        tile_edges.append(image.splitlines()[0])
        tile_edges.append(image.splitlines()[-1])
        tile_edges.append("".join([line[0] for line in image.splitlines()]))
        tile_edges.append("".join([line[-1] for line in image.splitlines()]))
        #print(tile_edges)
        tile_data[int(id_number)] = tile_edges
        all_edges += tile_edges
    #print(all_edges)
    corner_tiles = set()
    for tile in tile_data:
        #print(tile)
        edge_count = 0
        for edge in tile_data[tile]:
            edge_count += all_edges.count(edge)
            edge_count += all_edges.count(edge[::-1])
            #print(" ", edge, "-", edge[::-1], ":", edge_count)
        if edge_count < 7:
            corner_tiles.add(tile)
    pt1 = 1
    for val in corner_tiles:
        pt1 *= val
    #print(corner_tiles)
    print(f"Pt1: {pt1}")

if __name__ == "__main__":
    day20()
