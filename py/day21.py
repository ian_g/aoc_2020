#!/usr/bin/env python
import re
import sys

def day21():
    input_text = sys.stdin.read() 
    non_allergens, candidates = recipe_analysis(input_text)
    pt1 = 0
    pieces = input_text.split()
    for ingredient in non_allergens:
        pt1 += pieces.count(ingredient)
    print(f"Pt1: {pt1}")
    pt2 = specific_allergens(candidates)
    print(f"Pt2: {pt2}")

def recipe_analysis(input_text):
    all_allergens = set()
    all_ingredients = set()
    candidates = {}
    for food in input_text.splitlines():
        match = re.fullmatch("(.+) \(contains (.+)\)", food)
        if not match:
            all_ingredients = all_ingredients | set(food.split())
            continue
        ingredients = match.group(1)
        allergens = match.group(2)
        all_allergens = all_allergens | set(allergens.split(", "))
        for allergen in allergens.split(", "):
            if allergen in candidates:
                candidates[allergen] = candidates[allergen] & set(ingredients.split())
            else:
                candidates[allergen] = set(ingredients.split())
        all_ingredients = all_ingredients | set(ingredients.split())
    non_allergens = all_ingredients.difference(*candidates.values())
    return non_allergens, candidates

def specific_allergens(candidates):
    total_length = sum(len(value) for key, value in candidates.items())
    min_length = len(list(candidates.keys()))
    #Reduce to 1 per candidate
    while total_length > min_length:
        for key in candidates:
            if len(candidates[key]) == 1:
                for key2 in candidates:
                    if len(candidates[key2]) == 1: 
                        continue
                    candidates[key2] = candidates[key2] - candidates[key]
        total_length = sum(len(value) for key, value in candidates.items())

    ordered = sorted(list(candidates.keys()))
    allergens = []
    for key in ordered:
        allergens.append(candidates[key].pop())
    return ",".join(allergens)

if __name__ == "__main__":
    day21()
