#!/usr/bin/env python
import sys

def day13():
    [timestamp, buses] = [line.strip() for line in sys.stdin]

    #timestamp = "939"
    #buses = "7,13,x,x,59,x,31,19"

    timestamp = int(timestamp)
    bus_ids = [int(x) for x in buses.split(",") if x != "x"]
    soonest_trip = [0, timestamp + 1]
    for bus in bus_ids:
        leave_after = (timestamp//bus)*bus
        if leave_after < timestamp:
            leave_after += bus
        leave_after -= timestamp
        if leave_after < soonest_trip[1]:
            soonest_trip = [bus, leave_after]
    pt1 = soonest_trip[0]*soonest_trip[1]
    print(f"Pt1: {pt1}")

    buses = buses.split(",")
    bus_offsets = []
    for idx in range(len(buses)):
        if buses[idx] == "x":
            continue
        bus_offsets.append([int(buses[idx]), idx])

    time = 0
    increment_by = 1
    for bus in bus_offsets:
        while (time + bus[1]) % bus[0] != 0:
            time += increment_by
        increment_by *= bus[0]
    print(f"Pt2: {time}")

if __name__ == "__main__":
    day13()
