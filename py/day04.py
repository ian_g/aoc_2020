#!/usr/bin/env python
import sys

def day04():
    #Text from fileinput comes with "\n" at the end
    text = [line for line in sys.stdin]
    text = "".join(text)

    reqd = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    passports = text.split('\n\n')
    has_reqd_keys = 0
    has_valid_data = 0
    for passport in passports:
        field_keys = {field.split(":")[0] for field in passport.split()} 
        if field_keys & reqd == reqd:
            has_reqd_keys += 1
            has_valid_data += validated_data(passport)
            
    print(f"Pt1: {has_reqd_keys}")
    print(f"Pt2: {has_valid_data}")

def validated_data(passport):
    valid = True
    for field in passport.split():
        key = field[:3]
        data = field[4:]
        if not valid:
            return valid
        if key == "byr": 
            valid = valid and len(data) == 4 and "1920" <= data <= "2002"
        elif key == "iyr": 
            valid = valid and len(data) == 4 and "2010" <= data <= "2020"
        elif key == "eyr": 
            valid = valid and len(data) == 4 and "2020" <= data <= "2030"
        elif key == "hgt": 
            valid = valid and validate_hgt(data)
        elif key == "hcl": 
            valid = valid and validate_hcl(data)
        elif key == "ecl": 
            valid_values = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
            valid = valid and data in valid_values
        elif key == "pid": 
            valid = valid and len(data) == 9 and data.isdigit() 
        else:
            continue
    return valid

def validate_hcl(data):
    try:
        int(data[1:], 16)
    except ValueError:
        return False
    return data[0] == "#"

def validate_hgt(height):
    if "cm" in height:
        return "150" <= height[:-2] <= "193"
    elif "in" in height:
        return "59" <= height[:-2] <= "76"
    return False

if __name__ == "__main__":
    day04()
