#!/usr/bin/env python
import sys

def day06():
    input_text = "".join([line for line in sys.stdin])
    any_yes = []
    all_yes = []
    for group in input_text.split("\n\n"):
        group_sets = []
        for response in group.splitlines():
            group_sets.append(set(response))
        any_yes.append(set.union(*group_sets))
        all_yes.append(set.intersection(*group_sets))
    pt1 = sum([len(ans) for ans in any_yes])
    pt2 = sum([len(ans) for ans in all_yes])
    print(f"Pt1: {pt1}")
    print(f"Pt2: {pt2}")

if __name__ == "__main__":
    day06()
