#!/usr/bin/env python
import sys 

def day18():
    input_text = [line.strip() for line in sys.stdin]
    prec = {
        "(": (9, True),
        ")": (0, True),
        "^": (4, False),
        "*": (3, True),
        "+": (3, True),
        "-": (2, True),
        "/": (2, True),
    }
    pt1 = sum([evaluatePostfix(toPostfix(line, prec)) for line in input_text])
    print(f"Pt1: {pt1}")
    prec["+"] = (4, True)
    pt2 = sum([evaluatePostfix(toPostfix(line, prec)) for line in input_text])
    print(f"Pt2: {pt2}")

def evaluatePostfix(problem):
    problem = problem.split()
    operands = []
    for item in problem:
        if item.isdigit():
            operands.append(int(item))
        else: #Simplified because input already verified
            right = operands.pop()
            left = operands.pop()
            if item == "+":
                operands.append(right + left)
            elif item == "*":
                operands.append(right * left)
            elif item == "/":
                operands.append(right / left)
            elif item == "-":
                operands.append(right - left)
            else:
                raise ValueError("Unknown operator {item}")
    return operands[0]

def toPostfix(problem, precedence):
    for key in precedence:
        problem = f" {key} ".join(problem.split(key))
    problem = problem.split() 
    output_stack = []
    op_stack = []
    for token in problem:
        if token == "(":
            op_stack.append(token)
        elif token == ")":
            top_token = op_stack.pop()
            while top_token != "(":
                output_stack.append(top_token)
                top_token = op_stack.pop()
        elif token in precedence:
            if not op_stack:
                op_stack.append(token)
                continue
            poppable = op_stack[-1]
            while popoff(precedence[token], precedence[poppable]) and poppable != "(":
                output_stack.append(op_stack.pop())
                if not op_stack:
                    break
                poppable = op_stack[-1]
            op_stack.append(token)
        else:
            output_stack.append(token)
    while op_stack:
        output_stack.append(op_stack.pop())
    return " ".join(output_stack)

def popoff(token_prec, poppable_prec):
    if token_prec[1]: #Left associative
        return token_prec[0] <= poppable_prec[0]
    return token_prec[0] < poppable_prec[0]

if __name__ == "__main__":
    day18()
    #test()
