#!/usr/bin/env python
import sys 
import collections
STEPS = {
    "w": (-2, 0),
    "e": (2, 0),
    "n": {
        "w": (-1, 1),
        "e": (1, 1),
    },
    "s": {
        "w": (-1, -1),
        "e": (1, -1),
    },
}

def day24():
    flips = [line.strip() for line in sys.stdin]
    flipped = collections.defaultdict(int)
    for line in flips:
        flipped[tile_coord(line)] += 1
    pt1 = sum([1 for key in flipped if flipped[key] % 2 == 1])
    print(f"Pt1: {pt1}")

    black_tiles = {coord for coord in flipped if flipped[coord] % 2 == 1}
    for _ in range(100):
        black_tiles = flip_tiles(black_tiles)
    print(f"Pt2: {len(black_tiles)}")

def flip_tiles(black_tiles):
    coords = all_neighbors(black_tiles)
    new_black = set()
    for coord in coords:
        if score(coord, black_tiles) == 2:
            new_black.add(coord)
        elif score(coord, black_tiles) == 1 and coord in black_tiles:
            new_black.add(coord)
    return new_black

def score(coord, black):
    adjacent = neighbors(coord)
    return len((adjacent & black))

def all_neighbors(coords):
    adjacent = []
    for coord in coords:
        adjacent.append(neighbors(coord))
    return set.union(*adjacent)

def neighbors(coord):
    adjacent = set()
    adjacent.add(tuple(map(lambda x, y: x + y, coord, (-1, -1))))
    adjacent.add(tuple(map(lambda x, y: x + y, coord, (-1, 1))))
    adjacent.add(tuple(map(lambda x, y: x + y, coord, (-2, 0))))
    adjacent.add(tuple(map(lambda x, y: x + y, coord, (1, 1))))
    adjacent.add(tuple(map(lambda x, y: x + y, coord, (2, 0))))
    adjacent.add(tuple(map(lambda x, y: x + y, coord, (1, -1))))
    return adjacent

def tile_coord(line):
    steps = STEPS
    coords = (0, 0)
    for char in line:
        #print(char)
        if char in ["n", "s"]:
            steps = steps[char]
            #print(char, "skipping")
            continue
        #print(coords, steps[char])
        coords = tuple(map(lambda x, y: x + y, coords, steps[char]))
        steps = STEPS
    return coords

if __name__ == "__main__":
    day24()
