#!/usr/bin/env python
import re
import sys 

def day16():
    input_text = [line.strip() for line in sys.stdin]
    #Ticket: line of comma-separated values

    rules = {} 
    for line in input_text:
        if line == "":
            break
        fields =  re.split(": | or |-", line)
        rules[fields[0]] = [
            range(int(fields[1]), int(fields[2])+1),
            range(int(fields[3]), int(fields[4])+1),
        ]

    my_ticket = input_text[input_text.index("your ticket:")+1].split(",")

    start_idx = input_text.index("nearby tickets:")
    pt1 = 0
    valid_tickets = [my_ticket]
    for ticket in input_text[start_idx+1:]:
        error_value, valid = check_ticket(ticket.split(","), rules)
        if valid: 
            valid_tickets.append(ticket.split(","))
        pt1 += error_value
    print(f"Pt1: {pt1}")

    valid_values = [
        [int(ticket[idx]) for ticket in valid_tickets]
        for idx in range(len(valid_tickets[0]))
    ]

    ticket_fields = []
    for values in valid_values:
        ticket_fields.append(possible_fields(values, rules))
    ticket_fields = calculate_values(ticket_fields)

    pt2 = 1
    for idx in range(len(ticket_fields)):
        if "departure" in ticket_fields[idx]:
            pt2 *= int(my_ticket[idx]) 
    print(f"Pt2: {pt2}")

def calculate_values(fields):
    ticket_fields = [""]*len(fields)
    last = ""
    while "" in ticket_fields:
        for idx, values in enumerate(fields):
            values.discard(last)
            if len(values) == 1:
                value = fields[idx].pop()
                ticket_fields[idx] = value
        last = value
    return ticket_fields

def possible_fields(values, rules):
    possible_values = set(rules.keys())
    impossible_values = set()
    for key, rule in rules.items():
        for value in values:
            if (value not in rule[0]) and (value not in rule[1]):
                impossible_values.add(key)
                break
    return possible_values - impossible_values

def check_ticket(ticket, rules):
    error_value = 0
    error_found = False 
    for value in ticket:
        value = int(value)
        valid = False
        for rule in rules.values():
            fulfills_rule = (value in rule[0]) or (value in rule[1])
            valid = valid or fulfills_rule
        if not valid:
            error_found = True
            error_value += int(value)
    return error_value, not error_found 

if __name__ == "__main__":
    day16()
