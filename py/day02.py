#!/usr/bin/env python3
import sys

def day02():
    passwords = [line for line in sys.stdin]
    valid_sled_passwords = 0
    valid_toboggan_passwords = 0
    for pw in passwords:
        if valid_toboggan_password(pw):
            valid_toboggan_passwords += 1
        if valid_sled_password(pw):
            valid_sled_passwords += 1 

    print(f"Pt1: {valid_sled_passwords}")
    print(f"Pt2: {valid_toboggan_passwords}")

def valid_toboggan_password(password):
    pw = password.split(": ")[1]
    [low, high] = password.split()[0].split("-") 
    letter = password.split(": ")[0].split()[1]

    return (pw[int(low)-1] == letter) ^ (pw[int(high)-1] == letter)
    
def valid_sled_password(password):
    pw = password.split(": ")[1]
    [low, high] = password.split()[0].split("-") 
    letter = password.split(": ")[0].split()[1]

    count = pw.count(letter)
    return count >= int(low) and count <= int(high)

if __name__ == "__main__":
    day02()
