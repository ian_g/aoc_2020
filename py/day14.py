#!/usr/bin/env python
import sys 

def day14():
    input_text = [line.strip() for line in sys.stdin]

    program = []
    for line in input_text:
        if line[:4] == "mask":
            program.append(["mask", line[7:]])
        elif line[:3] == "mem":
            memadd = int(line.split("[")[1].split("]")[0])
            program.append([memadd, int(line.split(" = ")[1])])

    memory = run_v1(program)
    pt1 = sum(list(memory.values()))
    print(f"Pt1: {pt1}")

    memory = run_v2(program)
    pt2 = sum(list(memory.values()))
    print(f"Pt2: {pt2}")

def run_v1(program):
    memory = {}
    mask = "".join(["X"]*36) 
    for instruction in program:
        if instruction[0] == "mask":
            mask = instruction[1] 
            continue
        memval = list("{0:>036b}".format(instruction[1]))
        for (idx, val) in enumerate(mask):
            if val == "X":
                continue
            memval[idx] = val
        memory[instruction[0]] = int("".join(memval), 2)
    return memory
    
def run_v2(program):
    memory = {}
    mask = "".join(["0"]*36)
    for instruction in program:
        if instruction[0] == "mask":
            mask = instruction[1]
            continue
        memval = instruction[1]
        memadd = list("{0:>036b}".format(instruction[0]))
        for (idx, val) in enumerate(mask):
            if val == "0":
                continue
            elif val == "1":
                memadd[idx] = "1"
                continue
            memadd[idx] = "X"
        write_to(memadd, memval, memory)
    return memory

def write_to(address, value, memory):
    if "X" not in address:
        memadd = int("".join(address), 2)
        memory[memadd] = value
        return
    idx = address.index("X")
    address_one = address.copy()
    address_one[idx] = "1"
    address_zero = address.copy()
    address_zero[idx] = "0"
    write_to(address_one, value, memory)
    write_to(address_zero, value, memory)
    return
    

if __name__ == "__main__":
    day14()
